//add = (x, y) => {
exports.add = (x, y) => {
    //x + y;
    console.log(`sum of ${x} and ${y} is ${x+y}`);
};

//add(3,6);

exports.MyCircle = class {
// MyCircle = class {
    constructor(r) {
        this.radius = r;
        console.log("Hello I am My Circle Constructor...");
    }
    area() {
        let area = Math.PI * this.radius * this.radius;
        console.log(`Area: ${area.toFixed(2)}`);
    }
}

// myobj = new MyCircle(3);
// myobj.area();