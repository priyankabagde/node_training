const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

app.route('/')

.get((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.query;
        console.log(data);
        res.send(JSON.stringify(data));
})
.post((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.body;
        res.send(JSON.stringify(data));
})
.put((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.body;
        res.send(JSON.stringify(data));
})

app.listen(8989, () => {
console.log('Listening at port 8989');
});


/*
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

app.route('/')

.get((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.query;
        console.log(data);
        res.send(JSON.stringify(data));
})
.post((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.body;
        res.send(JSON.stringify(data));
})
.put((req, res) => {
        res.setHeader('Content-Type', 'application/json');
        data = req.body;
        res.send(JSON.stringify(data));
})

app.listen(8989, () => {
console.log('Listening at port 8989');
});
*/

/*
var express = require('express');
var app = express();
var student = require('./student');

app.use('/student', student);

app.use(function(req, res, next) {
        res.status(404);
        res.send('404: File Not Found');
});

app.listen(8989, () => {
console.log('Listening at port 8989');
});
*/

/*
var http = require("http");
http.createServer((req, res) => {
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.write("this is line 1");
        console.log(req.url);

        if(req.url == "/") {
                res.write("Home req hit</br>");
        } else if(req.url == "/teacher") {
                res.write("Teacher req hit</br>");
        } else if(req.url == "/student") {
                res.write("Student req hit</br>");
        } else {
                res.end("Page Not Found....");
        }

        res.end("hello world....");

}).listen(8989, () => {
        console.log("listening at port 8989…")
});
*/

/*
var mymath = require("./mymodule")

mymath.add(5,6);

cobj = new mymath.MyCircle(5);
cobj.area();
*/
console.log("hello tech team");